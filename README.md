<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<a href="https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege">
  <img src="images/logo.png" alt="Logo" width="80" height="80">
</a>

<h3>Trames de chapitres avec la classe Sesamanuel et le paquet ProfCollege</h3>

<p>
  Ce dépôt contient une trame exemple permettant d'utiliser la classe <a href="https://www.ctan.org/pkg/sesamanuel">sesamanuel</a> et le paquet <a href="https://www.ctan.org/pkg/profcollege">profcollege</a> .
  <br />  
  <br />
  <a href="https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/issues">Report Bug</a>
  ·
  <a href="https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/issues">Request Feature</a>
</p>

<!-- DOSSIER 3e_aaaa -->
## Dossier ./3e_aaaa
Trames vides pour des chapitres de 3e
<a href="https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/-/blob/master/3e_aaaa/README.md">README.md du dossier ./3e_aaaa</a>

**Remarque :** compilation LuaTeX obligatoire.

## Remerciements

- À Christophe POULAIN pour :
  - son aide précieuse de TeXnicien
  - son paquet [ProfCollege](https://www.ctan.org/pkg/ProfCollege)
  - sa [Base Collège](https://melusine.eu.org/lab/cp) partagée sur [Mélusine](https://melusine.eu.org/)
- À Jean-Côme CHAPENTIER pour la [classe sesamanuel](https://ctan.org/pkg/sesamanuel) réalisée pour l'association [Sésamath](https://www.sesamath.net/).
- À l'association Sésamath pour ces nombreux [manuels et cahiers](https://manuel.sesamath.net/) en partage libre.
- À Nathalie DAVAL, pour avoir attiré mon attention sur la [classe sesamanuel](https://ctan.org/pkg/sesamanuel).

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

Distributed under the CC BY-NC-SA 4.0 License. See [LICENSE](https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/-/blob/master/LICENCE.md) for more information.

<!-- MARKDOWN LINKS & IMAGES -->
[stars-shield]: https://img.shields.io/badge/STARS-2-blue
[stars-url]: https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/-/starrers
[issues-shield]: https://img.shields.io/badge/ISSUES-0-green
[issues-url]: https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/issues
[license-shield]: https://img.shields.io/badge/LICENCE-CC_BY--NC--SA_4.0-d5d8dc
[license-url]: https://forge.apps.education.fr/lozanosebastien/sesamanuel_profcollege/-/blob/master/LICENCE.md
