Ce dossier contient un master pour la réalisation des chapitres de college avec la classe [sesamanuel de l'association sesamath](https://www.ctan.org/pkg/sesamanuel) en incluant le paquet [ProfCollege de Christophe POULAIN](https://ctan.org/pkg/profcollege).

> Le mode de compilation à utiliser est exclusivement LuaLaTeX pour l'utilisation de certaines commandes du paquet **ProfCollege**.

# Objectifs

- Un chapitre témoin pour chaque thème

# Remarques 

- Compilation obligatoire en mode luaLaTeX

# Arborescence
## Dossiers du dépot

```bash
.
├── 0perso                          # Dossier contenant les fichiers de configuration persos
├── 3e_aaaa                         # Dossier pour les chapitres de 3e de l'année aaaa
│   ├── A1                          # Chapitre modèle pour le thème Algorithmique
│   │   ├── A1                      # Pour regrouper les fichiers de structure du chapitre
│   │   │   ├── images              # No comment
│   │   │   └── inc                 # Pour regrouper les fichiers du chapitre
│   │   └── corrections             # Pour récupérer les fichiers de correction générés du chapitre
│   ├── D1                          # Chapitre modèle pour le thème Gestion de données
│   │   ├── corrections             
│   │   └── D1                      
│   │       ├── images              
│   │       └── inc                 
│   ├── G1                          # Chapitre témoin de Géométrie
│   │   ├── corrections
│   │   └── G1
│   │       ├── images
│   │       └── inc
│   ├── M1                          # Chapitre témoin de Mesures et grandeurs
│   │   ├── corrections
│   │   └── M1
│   │       ├── images
│   │       └── inc
│   └── N1                          # Chapitre témoin de Numérique
│       ├── corrections
│       └── N1
│           ├── images
│           └── inc
└── images                          # No comment
```

### Détails pour un chapitre
Pour chaque chapitre, la structure est la même, le détail pour le chaptire témoin A1.

```bash
.
├── A1                                                  # Pour regrouper les fichiers de structure du chapitre 
│   ├── images                                          # No comment                    
│   │   └── compas.eps
│   ├── inc                                             # Pour regrouper les fichiers du chapitre
│   │   ├── activite001.tex                             # Fichier source avec le contenu 
│   │   ├── coursSection001.tex                         # Fichier source avec le contenu 
│   │   ├── coursSection002.tex                         # Fichier source avec le contenu 
│   │   ├── coursSection003.tex                         # Fichier source avec le contenu 
│   │   ├── coursSection004.tex                         # Fichier source avec le contenu 
│   │   ├── exosAppr001.tex                             # Fichier source avec le contenu 
│   │   ├── exosAppr002.tex                             # Fichier source avec le contenu 
│   │   ├── exosBase001.tex                             # Fichier source avec le contenu 
│   │   ├── exosBase002.tex                             # Fichier source avec le contenu 
│   │   ├── exosBase003.tex                             # Fichier source avec le contenu 
│   │   ├── exosBase004.tex                             # Fichier source avec le contenu 
│   │   ├── exosEnigme001.tex                           # Fichier source avec le contenu 
│   │   ├── prerequis001.tex                            # Fichier source avec le contenu 
│   │   └── prerequis_videos.tex                        # Fichier source avec le contenu 
│   ├── incActivites.tex                                # Fichier pour inclure les différents fichiers d'activités
│   ├── incChapitre.tex                                 # Fichier pour inclure les différentes éléments du chapitre
│   ├── incCours.tex                                    # Fichier pour inclure les différents fichiers de cours
│   ├── incExosApprofondissement.tex                    # Fichier pour inclure les différents fichiers de exos d'approfondissement
│   ├── incExosBase.tex                                 # Fichier pour inclure les différents fichiers de exos de base
│   ├── incPrerequis.tex                                # Fichier pour inclure les différents fichiers de prérequis
│   └── incRecreation.tex                               # Fichier pour inclure les différents fichiers d'activités récréatives
├── corrections
│   └── emptyFile.txt
└── seqxx-3e_aaaa_A1.tex                                # Fichier source à compiler
```
